const generateBlockedPositions = (width, height) => {
    const blocked = [];
    for (let i = 0; i < width; i++) {
        blocked[i] = [];
        for (let j = 0; j < height; j++) {
            blocked[i][j] = Math.floor(Math.random() * 10) > 7 ? true : undefined;
        }
    }
    return blocked;
};

const getPoint = (board, x, y) => {
    let point = {};
    if (x !== undefined && y !== undefined) {
        point = board.nodes[x][y];
    }
    return point;
}

export default {
    generateBlockedPositions,
    getPoint
}