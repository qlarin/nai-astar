import $ from 'jquery';
import 'jquery-ui-bundle';
import './static/styles.scss';
import Board from './logic/board.js';
import AStar from './logic/astar.js';
import generators from './utils/generators.js';

const width = 10;
const height = 10;
const nodeSize = 5;
const tileWidth = width * nodeSize;
const tileHeight = height * nodeSize;
let blocked = undefined;
let board = undefined;

$(function() {
    initPanel();
    initMap();
})

const initPanel = () => {
    const panel = $("#panel");
    const menu = $("<div />").attr('id', 'menu');
    const startingPoint = createDraggableElement("starting-point");
    const goalPoint = createDraggableElement("goal-point");
    const runButton = createButton('Run', 'run', run);
    const informationButton = createButton('Show info', 'info', showNodeInformations);
    const resetButton = createButton('Reset', 'reset', reset);
    menu.append(startingPoint);
    menu.append(goalPoint);
    menu.append(runButton);
    menu.append(informationButton);
    menu.append(resetButton);
    panel.append(menu);
}

const initMap = () => {
    blocked = generators.generateBlockedPositions(width, height);
    createMap();
}

const createMap = () => {
    board = new Board(width, height, blocked);
    const map = $("#board");
    const tile = $("<span />").addClass("tile")
        .width(tileWidth)
        .height(tileHeight);
    
    for (let x = 0; x < board.nodes.length; x++) {
        const row = $("<div />").addClass("row").css('min-width', width * tileWidth * 1.2);
        for (let y = 0; y < board.nodes[x].length; y++) {
            const col = tile.clone();
            col.attr("id", "tile-"+x+"-"+y)
                .attr("x", x)
                .attr("y", y);
            if (!board.nodes[x][y].walkable) {
                col.addClass("obstacle");
            } else {
                col.droppable({
                    drop: function(event, ui) {
                        attachElementPoint(ui.draggable, $(this));
                    }
                });
            }
            row.append(col);
        }
        map.append(row);
    }
}

const run = () => {
    if ($("#run").hasClass("lock")) {
        return false;
    }
    const algorythm = new AStar();
    const position = getNode(".start");
    const goal = getNode(".goal");
    if (Object.keys(position).length !== 0 && Object.keys(goal).length !== 0 && position !== goal) {
        const path = algorythm.searchPath(position, goal, board);
        drawPath(path);
        showNodeInformations('create');
        $("#run").addClass("lock");
        if (path.length === 0) {
            alert('Brak trasy.');
        }
    } else {
        alert('Aby rozpocząć ustaw punkty na planszy.');
    }
}

const reset = () => {
     $("#run").removeClass("lock");
     $("#board").empty();
     createMap();
}

const createButton = (text, id, method) => {
    return $("<button />", {
       text: text,
       id: id,
       click: method 
    });
}

const createDraggableElement = (identifier) => {
    return $("<div />").addClass(identifier)
        .width(tileWidth/2.2)
        .height(tileHeight/2.2)
        .draggable({
            cursor: 'move',
            helper: 'clone'
        });
}

const createInformationSpan = (text) => {
    return $("<span />").addClass("debug").text(text);
}

const attachElementPoint = (dropped, element) => {
    let point = '';
    if (dropped.hasClass('starting-point')) {
        point = 'start';
    } else if (dropped.hasClass('goal-point')) {
        point = 'goal';
    }
    $("."+point).removeClass(point);
    element.addClass(point);
}

const getNode = (element) => {
    let point = {};
    const x = $(element).attr('x');
    const y = $(element).attr('y');
    if (x && y) {
        point = generators.getPoint(board, x, y);
    }
    return point;
}

const drawPath = (path) => {
    for (let i = 0; i < path.length; i++) {
        move(i, path);
    }
}

const move = (i, path) => {
    setTimeout(function() {
        $('#tile-'+path[i].y+'-'+path[i].x).addClass('path');
    }, 500 * i);
}

const showNodeInformations = (action) => {
    if (action === 'create') {
        for (let x = 0; x < board.nodes.length; x++) {
            for (let y = 0; y < board.nodes[x].length; y++) {
                if (board.nodes[x][y].walkable && board.nodes[x][y].visited) {  
                    $('#tile-'+x+'-'+y)
                        .append(createInformationSpan("F: " + board.nodes[x][y].f))
                        .append(createInformationSpan("G: " + board.nodes[x][y].g))
                        .append(createInformationSpan("H: " + board.nodes[x][y].h));
                }
            }
        }
    } else {
        $(".debug").each(function () {
            $(this).toggleClass("show");
        });
    }
}
