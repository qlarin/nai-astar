import Heuristics from './heuristics.js';

class AStar {

    constructor(heuristic = Heuristics.manhattan) {
        this.heuristic = heuristic;
        this.defaultCost = 1;
    }
    
    searchPath(position, goal, board) {
        let openedNodes = new Array();
        openedNodes.push(position);

        while (openedNodes.length) {
            let lowestIndex = 0;
            lowestIndex = this.findLowest(openedNodes, lowestIndex);
            const node = openedNodes[lowestIndex];
            if (node.same(goal)) {
                return this.preparePath(node);
            }
            node.changeClosed(true);
            openedNodes = this.removeNode(openedNodes, lowestIndex);
            openedNodes = this.checkNeighbors(openedNodes, node, goal, board);
        }
        return [];
    }

    checkNeighbors(openedNodes, node, goal, board) {
        const neighbors = board.getNodeNeighbors(node);
        for (let i = 0; i < neighbors.length; i++) {
            const neighbor = neighbors[i];
            if (neighbor.closed) {
                continue;
            }
            const g = node.g + this.defaultCost;
            if (!neighbor.visited || g < neighbor.g) {
                neighbor.g = g;
                neighbor.h = neighbor.h || this.heuristic(neighbor, goal);
                neighbor.f = neighbor.g + neighbor.h;
                neighbor.changeParent(node);
                if (!neighbor.visited) {
                    neighbor.changeVisited(true);
                    openedNodes.push(neighbor);
                }
            }
        }
        return openedNodes;
    }

    removeNode(list, index) {
        list.splice(index, 1);
        return list;
    }

    findLowest(list, lowest) {
        let lowestIndex = lowest;
        for (let i = 0; i < list.length; i++) {
            if (list[i].f < list[lowestIndex].f) {
                lowestIndex = i;
            }
        }
        return lowestIndex;
    }

    preparePath(node) {
        const path = new Array();
        let current = node;

        while (current.parent) {
            path.push(current);
            current = current.parent;
        }
        return path.reverse();
    }
}
export default AStar;