const manhattan = (position, goal) => {
    const dx = Math.abs(goal.x - position.x);
    const dy = Math.abs(goal.y - position.y);
    return dx + dy;
};

export default {
    manhattan
}