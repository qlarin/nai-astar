class Node {

    constructor(x, y, walkable = true) {
        this.x = x;
        this.y = y;
        this.walkable = walkable;
        this.visited = false;
        this.closed = false;
        this.parent = null;
        this.f = 0;
        this.g = 0;
        this.h = 0;
    }

    changeWalkable = value => {
        this.walkable = value;
    }

    changeVisited = value => {
        this.visited = value;
    }

    changeParent = node => {
        this.parent = node;
    }

    changeClosed = value => {
        this.closed = value;
    }

    same = node => {
        return node === this;
    }

    toString() {
        return "G: " + this.g + " H: " + this.h + " F: " + this.f;
    }
}
export default Node;