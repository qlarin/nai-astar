import Node from './Node.js';

class Board {

    /**
    * @param {number} width ilość kolumn
    * @param {number} height ilość wierszy
    * @param {array} blocked tablica zablokowanych pól
    */
    constructor(width, height, blocked) {
        this.width = width;
        this.height = height;
        this.blocked = blocked;
        this.nodes = this.prepareNodes();
    }

    prepareNodes() {
        let nodes = new Array(this.height);
        nodes = this.createNodes(nodes);

        if (this.blocked !== undefined) {
            this.lockNodes(nodes);
        }

        return nodes;
    }

    createNodes(board) {
        const nodes = board;
        for (let y = 0; y < this.height; ++y) {
            nodes[y] = new Array(this.width);
            for (let x = 0; x < this.width; ++x) {
                nodes[y][x] = new Node(x, y);
            }
        }
        return nodes;
    }

    lockNodes(board) {
        const nodes = board;
        for (let y = 0; y < this.height; ++y) {
            for (let x = 0; x < this.width; ++x) {
                if (this.blocked[y][x]) {
                    nodes[y][x].changeWalkable(false);
                }
            }
        }
        return nodes;
    }

    getNodeNeighbors(node) {
        const neighbors = [];

        const north = this.getNeighborAtPosition(node.x, node.y - 1);
        if (north) {
            neighbors.push(north);
        }

        const south = this.getNeighborAtPosition(node.x, node.y + 1);
        if (south) {
            neighbors.push(south);
        }

        const west = this.getNeighborAtPosition(node.x - 1, node.y);
        if (west) {
            neighbors.push(west);
        }

        const east = this.getNeighborAtPosition(node.x + 1, node.y);
        if (east) {
            neighbors.push(east);
        }

        return neighbors;
    }

    getNeighborAtPosition(x, y) {
        let neighbor = undefined;
        if (this.isPositionWalkable(x, y)) {
            neighbor = this.getNodeAtPosition(x, y);
        }
        return neighbor;
    }    

    getNodeAtPosition(x, y) {
        return this.nodes[y][x];
    }

    changeWalkablePosition(x, y, value) {
        this.nodes[y][x].changeWalkable(value);
    }

    isPositionOnBoard(x, y) {
        return (x < this.width && x >= 0) && (y < this.height && y >= 0);
    }

    isNodeWalkableAtPosition(x, y) {
        return this.nodes[y][x].walkable;
    }

    isPositionWalkable(x, y) {
        return this.isPositionOnBoard(x, y) && this.isNodeWalkableAtPosition(x, y);
    }
}
export default Board;