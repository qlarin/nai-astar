var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: [
    './src/app.js',
  ],
  output: {
      path: path.resolve(__dirname, "src/build"),
      publicPath: '/build/',
      filename: 'bundle.js'
  },
  debug: true,
  devtool: 'source-map',
  module: {
    loaders: [
      { 
        test: /\.js$/,
        include: path.join(__dirname, 'src'),
        loader: 'babel-loader',
        query: {
          presets: ['es2015', 'stage-2']
        }
      },
      { 
        test: /\.scss$/,
        include: path.join(__dirname, 'src/static'),
        loader: "style!css?sourceMap!autoprefixer!less!sass?sourceMap"
      },
    ]
  },
  sassLoader: {
    includePaths: ['src/static']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase: "./src",
    host: '0.0.0.0',
    port: 8080
  }
};