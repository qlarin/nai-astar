# nai-astar

## Przykładowa implementacja algorytmu A* wraz z minimalistyczną wizualizacją

---

> Algorytm A* – algorytm heurystyczny znajdowania najkrótszej ścieżki w grafie
> ważonym z dowolnego wierzchołka do wierzchołka spełniającego określony warunek
> zwany testem celu. Algorytm jest zupełny i optymalny, w tym sensie, że 
> znajduje ścieżkę, jeśli tylko taka istnieje, i przy tym jest to ścieżka
> najkrótsza. Stosowany głównie w dziedzinie sztucznej inteligencji do 
> rozwiązywania problemów i w grach komputerowych do imitowania inteligentnego 
> zachowania. Źródło: https://pl.wikipedia.org/wiki/Algorytm_A*

---

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# generate production build
npm run prod

# watch changes in build
npm run watch
```
